import * as video from '../../utils/video';
const WXAPI = require('apifm-wxapi')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videos: [
      {
        index: 0,
        attr: {
          src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
          cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg'          
        }
      },
      {
        index: 1,
        attr: {
          src: 'http://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f30.mp4',
          cover:'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg'
        }
      }
    ],
    courses:[
      {id:1, name:'第一节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100, goodsId: 785722, number:1 },
      {id:2, name:'第二节', src: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100      },
      {id:3, name:'第三节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100      },
      {id:4, name:'第四节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100      },
      {id:5, name:'第五节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100      },
      {id:6, name:'第六节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100      },
      {id:7, name:'第七节', src: 'https://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f40.mp4',
      cover: 'http://img.zcool.cn/community/014056564bd8596ac7251c94eb5559.jpg', price: 100     }
    ],
  },
  onLoad: function(e) {
    var course = JSON.parse(e.course)
    this.setData({
      curcourse: course
    })
  },
  onplay: function(e) {
    this.context = wx.createVideoContext('video')
    if (this.context) {
      // this.setData({
      //   currentVideo: this.data.courses[e.currentTarget.dataset.index]
      // })
      this.context.play()
    }
  },
  //主题封面点击-播放视频
  bindplay(e) {
    video.bindplay(this, e)
  },
  //监听视频播放
  bindplay_video(e) {
    video.bindplay_video(this, e)
  },
  //跳转到全屏播放页面
  startOnPlay(ev) {
    wx.navigateTo({
      url: '/pages/videoFull/videoFull?src=' + ev.currentTarget.dataset.src,
    })
  },
  bindPurchase: function() {
    const token = wx.getStorageSync('token')
    // WXAPI.wxpay({
    //   token: token,
    //   money: 100,
    //   payName: '支付测试'
    // }).then(function (res) {
    //   console.log(res)
    //   // 调起微信支付
    //   wx.requestPayment({
    //     timeStamp: res.data.timeStamp,
    //     nonceStr: res.data.nonceStr,
    //     package: 'prepay_id=' + res.data.prepayId,
    //     signType: 'MD5',
    //     paySign: res.data.sign,
    //     fail: function (aaa) {
    //       wx.showToast({
    //         title: '支付失败:' + aaa
    //       })
    //     },
    //     success: function () {
    //       wx.showToast({
    //         title: '支付成功'
    //       })
    //     }
    //   })
    // })
    var buyNowInfo = this.buildBuyNowInfo()
    var data = JSON.stringify(buyNowInfo)
    wx.navigateTo({
      url: '/pages/to-pay-order/index?data=' + data,
    })
  },
  buildBuyNowInfo: function() { 
    var shopCarMap = {};
    shopCarMap.name = this.data.curcourse.name
    shopCarMap.price = this.data.curcourse.price
    shopCarMap.goodsId = this.data.curcourse.goodsId
    shopCarMap.number = this.data.curcourse.number
    var buyNowInfo = {};
    buyNowInfo.shopNum = 0;
    buyNowInfo.shopList = [];
    buyNowInfo.shopList.push(shopCarMap);
    return buyNowInfo;
  }
})