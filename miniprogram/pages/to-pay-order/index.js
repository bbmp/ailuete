// pages/to-pay-order/index.js
const WXAPI = require('apifm-wxapi')
const AUTH = require('../../utils/auth')
const wxpay = require('../../utils/pay.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    coupons: [],
    goodsList: [],
    allGoodsAndYunPrice: 0,
    goodsJsonStr: "",
    bindMobileStatus: 0,
    couponAmount: 0, //优惠券金额
    curCoupon: null, // 当前选择使用的优惠券
    curCouponShowText: '请选择使用优惠券', // 当前选择使用的优惠券
    viewHeight: app.globalData.screenHeight,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = JSON.parse(options.data);
    this.data.goodsList = data.shopList;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    AUTH.checkHasLogined().then(isLogined => {
      if (isLogined) {
        this.doneShow()
      } else {
        AUTH.authorize().then(res => {
          this.doneShow()
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  async getUserApiInfo() {
    const res = await WXAPI.userDetail(wx.getStorageSync('token'))
    if (res.code == 0) {
      this.setData({
        bindMobileStatus: 1, //res.data.base.mobile ? 1: 2, // 账户绑定的手机号码状态
        mobile: res.data.base.mobile,
      })
    }
  },
  async doneShow() {
    this.getUserApiInfo()
    this.setData({
      goodsList: this.data.goodsList
    })
    this.processYunfei();
  },
  createOrder: function (e) {
    var that = this;
    var loginToken = wx.getStorageSync('token') // 用户登录 token

    let postData = {
      token: loginToken,
      goodsJsonStr: that.data.goodsJsonStr
    };
 
    if (that.data.curCoupon) {
      postData.couponId = that.data.curCoupon.id;
    }
    if (!e) {
      postData.calculate = "true";
    } else {
    }

    WXAPI.orderCreate(postData).then(function (res) {
      that.data.pageIsEnd = true
      if (res.code != 0) {
        that.data.pageIsEnd = false
        wx.showModal({
          title: '错误',
          content: res.msg,
          showCancel: false
        })
        return;
      }

      if (!e) {
        let hasNoCoupons = true
        let coupons = null
        if (res.data.couponUserList) {
          hasNoCoupons = false
          res.data.couponUserList.forEach(ele => {
            let moneyUnit = '元'
            if (ele.moneyType == 1) {
              moneyUnit = '%'
            }
            if (ele.moneyHreshold) {
              ele.nameExt = ele.name + ' [消费满' + ele.moneyHreshold + '元可减' + ele.money + moneyUnit +']'
            } else {
              ele.nameExt = ele.name + ' [减' + ele.money + moneyUnit + ']'
            }
          })
          coupons = res.data.couponUserList
        }
        
        that.setData({
          totalScoreToPay: res.data.score,
          allGoodsAndYunPrice: res.data.amountReal,
          yunPrice: res.data.amountLogistics,
          hasNoCoupons,
          coupons,
          couponAmount: res.data.couponAmount
        });
        that.data.pageIsEnd = false
        return;
      }
      that.processAfterCreateOrder(res)
    })
  },
  async processAfterCreateOrder(res) {
    // 直接弹出支付，取消支付的话，去订单列表
    const balance = this.data.balance
    
      // 没余额
    wxpay.wxpay('order', res.data.amountReal, res.data.id, "/pages/order-list/index");
    
  },
  bindChangeCoupon: function (e) {
    const selIndex = e.detail.value;
    this.setData({
      curCoupon: this.data.coupons[selIndex],
      curCouponShowText: this.data.coupons[selIndex].nameExt
    });
    this.processYunfei()
  },
  processYunfei() {
    var goodsList = this.data.goodsList
    if (goodsList.length == 0)
      return
    const goodsJsonStr = []
    for (let i = 0; i < goodsList.length; i++) {
      let carShopBean = goodsList[i];
      if (carShopBean.logistics || carShopBean.logisticsId) {
        isNeedLogistics = 1;
      }

      const _goodsJsonStr = {
        propertyChildIds: carShopBean.propertyChildIds
      }
      _goodsJsonStr.goodsId = carShopBean.goodsId
      _goodsJsonStr.number = carShopBean.number
      _goodsJsonStr.logisticsType = 0
      goodsJsonStr.push(_goodsJsonStr)
    }
    this.setData({
      goodsJsonStr: JSON.stringify(goodsJsonStr)
    });
    this.createOrder()
  },

  async goCreateOrder(){
    // 检测实名认证状态
    if (wx.getStorageSync('needIdCheck') == 1) {
      console.log(123);
      
      const res = await WXAPI.userDetail(wx.getStorageSync('token'))
      if (res.code == 0 && !res.data.base.isIdcardCheck) {
        wx.navigateTo({
          url: '/pages/idCheck/index',
        })
        return
      }
    }
    const subscribe_ids = wx.getStorageSync('subscribe_ids')
    if (subscribe_ids) {
      wx.requestSubscribeMessage({
        tmplIds: subscribe_ids.split(','),
        success(res) {
          
        },
        fail(e) {
          console.error(e)
        },
        complete: (e) => {
          this.createOrder(true)
        },
      })
    } else {
      this.createOrder(true)
    }    
  }
})