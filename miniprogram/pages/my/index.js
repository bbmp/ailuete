// pages/my/index.js
const WXAPI = require('apifm-wxapi')
const AUTH = require('../../utils/auth')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfoStatus: 1
  },

  updateUserInfo: function(e) {
    if (!e.detail.errMsg || e.detail.errMsg != "getUserInfo:ok") {
      wx.showModal({
        title: '提示',
        content: e.detail.errMsg,
        showCancel: false
      })
      return;
    }
    
    this.login()
    
    // app.globalData.userInfoStatus = 2
    // this.setData({
    //   userinfo: e.detail.userInfo,
    //   userInfoStatus: app.globalData.userInfoStatus
    // })
  },
  async login() {
    var that = this
    AUTH.checkHasLogined().then(isLogined => {
      if (!isLogined) {
        AUTH.login(that)
      }
    })
  },
  bindinfo: function(e) {
    wx.navigateTo({
      url: '/pages/info/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

      
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const _this = this
    AUTH.checkHasLogined().then(isLogined => {
      if (isLogined && app.globalData.userInfoStatus != 2) {
        _this.getUserApiInfo();
        return
      }
    })
    this.data.userInfoStatus = app.globalData.userInfoStatus
    this.setData({
      userInfoStatus: app.globalData.userInfoStatus,
      userinfo: app.globalData.userInfo
    })
  },

  async getUserApiInfo() {
    wx.showLoading({
      mask: true,
    })
    const res = await WXAPI.userDetail(wx.getStorageSync('token'))
    if (res.code == 0) {
      app.globalData.userInfoStatus = 2
      this.setData({
        userinfo: res.data.base,
        userInfoStatus: app.globalData.userInfoStatus
      })
    }
    wx.hideLoading()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindcoupon: function(e) {
    wx.navigateTo({
      url: '/pages/coupons/index',
    })
  },
  bindabout: function(e) {
    wx.navigateTo({
      url: '/pages/about/index',
    })
  },
  bindorder: function(e) {
    wx.navigateTo({
      url: '/pages/order-list/index',
    })
  } 
})