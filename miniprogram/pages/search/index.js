const WXAPI = require('apifm-wxapi')

// pages/search/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tuijian:[
      {id:1, name:'小学语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:2, name:'初中语数春季班', desc:'16课时', detail:'35类阅读写作', moneyMin:'10', moneyMax:'600'},
      {id:3, name:'中考冲刺班', desc:'16课时', detail:'60个考点复习', moneyMin:'10', moneyMax:'600'}
    ],
    loadingMoreHidden: true,
    page: 1 // 读取第几页
  },
  async search(e) {
    const inputVal = e.detail
    wx.showLoading({
      title: '加载中',
    })
    const _data = {
      orderBy: this.data.orderBy,
      page: this.data.page,
      pageSize: 20,
    }
    if (e.detail) {
      _data.k = e.detail
    }
    const res = await WXAPI.goods(_data)
    wx.hideLoading()
    if (res.code == 0) {
      if (this.data.page == 1) {
        this.setData({
          courses: res.data,
        })
      } else {
        this.setData({
          courses: this.data.goods.concat(res.data),
        })
      }
    } else {
      if (this.data.page == 1) {
        this.setData({
          courses: null,
        })
      } else {
        wx.showToast({
          title: '没有更多了',
          icon: 'none'
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getCourse: function(e) {
    wx.navigateTo({
      url: '/pages/detail/index',
    })
  }
})