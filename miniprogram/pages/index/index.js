const WXAPI = require('apifm-wxapi')
Page({
  data: {
    images:{},
    noticeList:{},
    tabs:[],
    tuijian:[
      {id:1, name:'小学语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:2, name:'初中语数春季班', desc:'16课时', detail:'35类阅读写作', moneyMin:'10', moneyMax:'600'},
      {id:3, name:'中考冲刺班', desc:'16课时', detail:'60个考点复习', moneyMin:'10', moneyMax:'600'},
      {id:4, name:'小学语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:5, name:'初中语数春季班', desc:'16课时', detail:'35类阅读写作', moneyMin:'10', moneyMax:'600'},
      {id:6, name:'中考冲刺班', desc:'16课时', detail:'60个考点复习', moneyMin:'10', moneyMax:'600'}
    ],
    xiaoxue:[
      {id:1, name:'小学语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:2, name:'小学英语春季班', desc:'16课时', detail:'英语口语', moneyMin:'10', moneyMax:'600'},
      {id:3, name:'小学练字班', desc:'16课时', detail:'练字', moneyMin:'10', moneyMax:'600'}
    ],
    chuzhong:[
      {id:1, name:'初中语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:2, name:'初中英语春季班', desc:'16课时', detail:'英语口语', moneyMin:'10', moneyMax:'600'},
      {id:3, name:'中考冲刺班', desc:'16课时', detail:'60个考点复习', moneyMin:'10', moneyMax:'600'}
    ],
    gaozhong:[
      {id:1, name:'高中语数春季班', desc:'16课时', detail:'20中思维训练方法', moneyMin:'10', moneyMax:'600'},
      {id:2, name:'高中英语春季班', desc:'16课时', detail:'英语口语', moneyMin:'10', moneyMax:'600'},
      {id:3, name:'高考冲刺班', desc:'16课时', detail:'60个考点复习', moneyMin:'10', moneyMax:'600'}
    ],
    currentItem: 0,
    activeIndex: 0,
    tabTop: 0,
    loadingMoreHidden: false,
    newcourses: [],
    courses: [],
    goodsList: []
  },
  onLoad: function () {
    let _this = this;
    const query = wx.createSelectorQuery();
    query.select('#tabs').boundingClientRect(function (res) {
      _this.data.tabTop = res.top//255
    }
    ).exec();
    // this.socketSend();
    this.initBanners();
    this.categories();
    this.getNotice();
    wx.cloud.callFunction({
      name: 'login',
      success: function(res){
        console.log(res)
      },
      fail: function(res) {
        console.log(res)
      }
    })
    wx.cloud.callFunction({
      name: 'echo',
      success: function(res) {
        console.log(res)
      },
      fail: function(res) {
        console.log(res)
      }
    })
  },

  onShow: function () {
    
  },
  onPageScroll: function (e) {
    console.log(e)
    console.log(this.data.tabTop)
    if (e.scrollTop > this.data.tabTop) {
      if (this.data.tabFix) {
        return
      }
      else {
        this.setData({
          tabFix: "Fixed"//添加吸顶类
        })
      }
    }
    else {
      this.setData({
        tabFix: ''
      })
    }
  },
  tapBanner: function(e) {
    const url = e.currentTarget.dataset.url
    if (url) {
      wx.navigateTo({
        url
      })
    }
  },
  goSearch(){
    wx.navigateTo({
      url: '/pages/search/index'
    })
  },
  tabClick: function(e) {
    this.data.activeIndex = e.detail.index;
  
    this.setCourses()
    this.setData({
      courses: this.data.courses,
      newcourses: this.data.newcourses
    })
  },
  getCourse: function(e) {
    var course = this.buildCourseInfo(e)
    
    wx.navigateTo({
      url: '/pages/detail/index?course=' + JSON.stringify(course),
    })
  },
  async socketSend() {
    let socketOpen = false
    let socketMsgQueue = [12]

    wx.onSocketOpen(function(res) {
      socketOpen = true
      for (let i = 0; i < socketMsgQueue.length; i++){
        sendSocketMessage(socketMsgQueue[i])
      }
      socketMsgQueue = []
    })
    //断开时的动作
    wx.onSocketClose(() => {
      console.log('WebSocket 已断开')
    })
    //报错时的动作
    wx.onSocketError(error => {
      console.log('socket error:' + error)
    })
    wx.connectSocket({
      url: 'wss://10.0.0.172:80',
      success: function(res) {
        console.log('success')
      },
      fail: function(res) {
        console.log('fail')
      }
    })
    function sendSocketMessage(msg) {
      if (socketOpen) {
        wx.sendSocketMessage({
          data:msg
        })
      } else {
        socketMsgQueue.push(msg)
      }
    }
  },
  async initBanners() {
    var that = this
    WXAPI.banners().then(res=>{
      if (res.code == 0) {
        that.setData({
          images: res.data
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '请在后台添加 banner 轮播图片',
          showCancel: false
        })
      }
    })  
  },
  async categories() {
    var that = this
    await WXAPI.goodsCategory().then(res=>{
      if (res.code == 0) {
        that.data.tabs = res.data
        that.data.activeIndex = 0
      }
    })
    this.getGoodsList(0)
  },
  async getGoodsList(categoryId) {
    var that = this
    wx.showLoading({
      mask: true,
    })
    const res = await WXAPI.goods({

    })
    wx.hideLoading()
    if (res.code == 0) {
      this.data.goodsList = res.data
    } else {
      this.setData({
        loadingMoreHidden: false
      })
      return
    }
    
    this.setCourses()

    this.setData({
      tabs: that.data.tabs,
      courses: that.data.courses,
      newcourses: that.data.newcourses
    })
  },
  getNotice:  function() {
    var that = this;
    WXAPI.noticeList({pageSize: 3}).then(res=>{
      if (res.code == 0) {
        that.setData({
          noticeList: res.data
        })
      }
    })
  },
  setCourses: function() {
    this.data.courses = []
    this.data.newcourses = []
    for (var i =0; i < this.data.goodsList.length; i++) {
      if (this.data.tabs[this.data.activeIndex].id == this.data.goodsList[i].categoryId) {
        if (this.data.goodsList[i].tags == 'new')
          this.data.newcourses.push(this.data.goodsList[i])
        else
          this.data.courses.push(this.data.goodsList[i])
      }
    }
  },
  goNotice(e) {
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/notice/notice?id=' + id,
    })
  },
  buildCourseInfo: function(e) { 
    var goodid = e.currentTarget.dataset.id
    var curcourse = {}
    for (let i = 0; i < this.data.newcourses.length; i++) {
      if (this.data.newcourses[i].id == goodid) {
        curcourse = this.data.newcourses[i]
        break
      }
    }
    for (let i = 0; i < this.data.courses.length; i++) {
      if (this.data.courses[i].id == goodid) {
        curcourse = this.data.courses[i]
        break
      }
    }
    var course = {}
    course.goodsId = curcourse.id
    course.name = curcourse.name
    course.number = 1
    course.price = curcourse.minPrice
    course.src = "http://1256993030.vod2.myqcloud.com/d520582dvodtransgzp1256993030/7732bd367447398157015849771/v.f30.mp4"
    course.cover = curcourse.pic
    course.desc = curcourse.characteristic

    return course
  }
});