//app.js
const WXAPI = require('apifm-wxapi')
const CONFIG = require('config.js')
const AUTH = require('/utils/auth')
//wx38a146b23f4a323e
App({
  onLaunch: function () {
    this.globalData = {}
    // this.getHeight();
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        env: 'cloud1-9gw7wb5obcedbfd3',
        traceUser: true,
      })
      WXAPI.init(CONFIG.subDomain)
    }
  },
  onShow: function() {
    // wx.login({
    //   success (res) {
    //     if (res.code == 0) {

    //     } else {
    //       console.log('登录失败!' + res.errMsg)
    //     }
    //   }
    // })
    // 自动登录
    AUTH.checkHasLogined().then(isLogined => {
      if (!isLogined) {
        // AUTH.autologin()
      } else {
        this.getUserApiInfo()
      }
    })
  },
  async getUserApiInfo() {
    const res = await WXAPI.userDetail(wx.getStorageSync('token'))
    if (res.code == 0) {
      this.globalData.userInfoStatus = 2
      this.globalData.userInfo = res.data.base
    }
  },
  async getHeight() {
    var that = this
    wx.getSystemInfo({
      success: res => {
        //导航高度
        that.globalData.navHeight = res.statusBarHeight ;
        // 获取视口高度
        let clientHeight = res.windowHeight;
        let clientWidth = res.windowWidth;
        let ratio = 750 / clientWidth;
        let height = clientHeight * ratio; 
        let modelmes = res.model;
        // 得到安全区域高度
      
        that.globalData.screenHeight = (res.safeArea.height - res.statusBarHeight) * ratio
      
      }, fail(err) {

      }
    })
  }
})
