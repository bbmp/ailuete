module.exports = {
  version: '10.5.1',
  note: '代码包', // 这个为版本描述，无需修改
  subDomain: '349af5409d79cda9b9c9293eb5b4270c', // 此处改成你自己的专属域名。什么是专属域名？请看教程 https://www.it120.cc/help/qr6l4m.html
  sdkAppID: 1400450467, // 腾讯实时音视频应用编号，请看教程 https://www.it120.cc/help/nxoqsl.html
}